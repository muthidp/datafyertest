﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DataFyerTest.Helper;
using DataFyerTest.Models;
using Microsoft.Win32;

namespace DataFyerTest
{
    /// <summary>
    /// Interaction logic for TestWindow.xaml
    /// </summary>
    public partial class TestWindow : Window
    {
        #region Properties
        private InterviewLogic _interview;

        private Person _person = null;

        private const string filterCsv = "CSV files (*.csv)|*.csv";
        #endregion

        public TestWindow()
        {
            InitializeComponent();
            Initialize();
            WindowState = WindowState.Maximized;
        }

        #region Common+
        private void Initialize()
        {
            ComboBoxFilterOperation.ItemsSource = new List<string>()
            {
                "AND",
                "OR"
            };
            ComboBoxFilterOperation.SelectedIndex = 0;

            ComboBoxLimitRows.ItemsSource = new List<int>()
            {
                10,
                25,
                50,
                100,
                1000
            };
            ComboBoxLimitRows.SelectedIndex = 0;

            PeopleDataGrid.SelectionChanged += (sender, args) =>
            {
                if (args.AddedItems != null && args.AddedItems.Count > 0)
                {
                    var selectedPerson = args.AddedItems[0] as Person;
                    SetDetail(selectedPerson);
                }
            };

            GridPeopleTable.Visibility = Visibility.Hidden;
        }

        private void SetDetail(Person person)
        {
            LabelPreviewGivenName.Content = person.GivenName;
            LabelPreviewMiddleName.Content = person.MiddleInitial;
            LabelPreviewSurname.Content = person.Surname;
            LabelPreviewGender.Content = person.Gender;
            LabelPreviewEmail.Content = person.EmailAddress;
            LabelPreviewCompany.Content = person.Company;
            LabelPreviewOccupation.Content = person.Occupation;
            LabelPreviewBloodType.Content = person.BloodType;
            LabelPreviewTitle.Content = person.Title;
        }
        #endregion

        #region Button event clicked
        private void OnButtonChooseFileClicked(object sender, RoutedEventArgs e)
        {
            var dlg = new Microsoft.Win32.OpenFileDialog
            {
                DefaultExt = ".csv",
                Filter = filterCsv
            };

            var result = dlg.ShowDialog();

            if (result == true)
            {
                string filename = dlg.FileName;
                TextBlockFileName.Text = filename;

                _interview = new InterviewLogic();
                _interview.ReadFromCsv(filename);

                FilterPeopleDataGrid();

                GridPeopleTable.Visibility = Visibility.Visible;
            }
        }

        private void OnButtonApplyFilterClicked(object sender, RoutedEventArgs e)
        {
            FilterPeopleDataGrid();
        }

        private void OnButtonExportCsvClicked(object sender, EventArgs e)
        {
            if (_interview?.People == null) return;

            var dialog = new SaveFileDialog()
            {
                DefaultExt = ".csv",
                Filter = filterCsv
            };

            if (dialog.ShowDialog() == true)
            {
                var filePath = dialog.FileName;
                PeopleDataGrid.SelectAllCells();
                PeopleDataGrid.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
                ApplicationCommands.Copy.Execute(null, PeopleDataGrid);
                PeopleDataGrid.UnselectAllCells();
                var data = (string)Clipboard.GetData(DataFormats.CommaSeparatedValue);
                File.WriteAllBytes(filePath, ExcelHelper.exportCSV(data));

                var dir = System.IO.Path.GetDirectoryName(filePath);
                OpenWindowExplorer(dir);
            }
        }

        private void OnButtonPrintClicked(object sender, EventArgs e)
        {
            if (_interview?.People == null) return;

            var printDlg = new PrintDialog();
            printDlg.PrintVisual(PeopleDataGrid, "nSlide Printing");
        }

        private void OnButtonAddNewClicked(object sender, EventArgs e)
        {
            var popup = new PersonDialog();
            popup.Closed += new EventHandler(DialogClose);
            popup.Show();
        }

        public void DialogClose(object sender, EventArgs args)
        {
            var personDialog = sender as PersonDialog;
            if (personDialog == null) return;

            if (!personDialog.IsSave) return;

            var newPerson = personDialog?.GetPerson();
            if (newPerson!=null)
            {
                _interview.People.Add(newPerson);
                PeopleDataGrid.ItemsSource = _interview.People;
                PeopleDataGrid.Items.Refresh();

                SetDetail(newPerson);
            }
        }

        private void ComboBoxLimitRows_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            FilterPeopleDataGrid();
        }

        private void ComboBoxFilterOperation_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            FilterPeopleDataGrid();
        }

        //private void OnTextBoxLimitRowsChanged(object sender, TextChangedEventArgs e)
        //{
        //    var textBox = sender as TextBox;
        //    var selectionStart = TextBoxLimitRows.SelectionStart;
        //    var newText = String.Empty;
        //    int count = 0;
        //    foreach (var c in TextBoxLimitRows.Text.ToCharArray())
        //    {
        //        if (Char.IsDigit(c) || Char.IsControl(c) || (c == '.' && count == 0))
        //        {
        //            newText += c;
        //            if (c == '.')
        //                count += 1;
        //        }
        //    }
        //    TextBoxLimitRows.Text = newText;
        //    TextBoxLimitRows.SelectionStart = selectionStart <= textBox.Text.Length ? selectionStart : textBox.Text.Length;
        //}


        private void StackPanel_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                FilterPeopleDataGrid();
            }
        }

        #endregion


        private void FilterPeopleDataGrid()
        {
            if (_interview?.People == null) return;

            var filteredPeople = _interview.People.AsQueryable();

            var operation = ComboBoxFilterOperation.SelectedValue.ToString();
            switch (operation)
            {
                case "AND":
                    if (!string.IsNullOrEmpty(TextBoxGender.Text))
                    {
                        filteredPeople = filteredPeople.Where(p => String.Equals(p.Gender, TextBoxGender.Text, StringComparison.CurrentCultureIgnoreCase));
                    }

                    if (!string.IsNullOrEmpty(TextBoxTitle.Text))
                    {
                        filteredPeople = filteredPeople.Where(p => String.Equals(p.Title, TextBoxTitle.Text, StringComparison.CurrentCultureIgnoreCase));
                    }

                    if (!string.IsNullOrEmpty(TextBoxOccupation.Text))
                    {
                        filteredPeople = filteredPeople.Where(p => String.Equals(p.Occupation, TextBoxOccupation.Text, StringComparison.CurrentCultureIgnoreCase));
                    }

                    if (!string.IsNullOrEmpty(TextBoxGivenName.Text))
                    {
                        filteredPeople = filteredPeople.Where(p => p.GivenName.ToLower().Contains(TextBoxGivenName.Text.ToLower()));
                    }

                    if (!string.IsNullOrEmpty(TextBoxMiddleName.Text))
                    {
                        filteredPeople = filteredPeople.Where(p => p.MiddleInitial.ToLower().Contains(TextBoxMiddleName.Text.ToLower()));
                    }

                    if (!string.IsNullOrEmpty(TextBoxSurname.Text))
                    {
                        filteredPeople = filteredPeople.Where(p => p.Surname.ToLower().Contains(TextBoxSurname.Text.ToLower()));
                    }

                    if (!string.IsNullOrEmpty(TextBoxCompany.Text))
                    {
                        filteredPeople = filteredPeople.Where(p => String.Equals(p.Company, TextBoxCompany.Text, StringComparison.CurrentCultureIgnoreCase));
                    }

                    if (!string.IsNullOrEmpty(TextBoxBloodType.Text))
                    {
                        filteredPeople = filteredPeople.Where(p => String.Equals(p.BloodType, TextBoxBloodType.Text, StringComparison.CurrentCultureIgnoreCase));
                    }

                    if (!string.IsNullOrEmpty(TextBoxEmail.Text))
                    {
                        filteredPeople = filteredPeople.Where(p => p.EmailAddress.ToLower().Contains(TextBoxEmail.Text.ToLower()));
                    }

                    break;

                case "OR":

                    var predicate = PredicateBuilder.False<Person>();

                    if (!string.IsNullOrEmpty(TextBoxGender.Text))
                    {
                        predicate = predicate.Or(p => String.Equals(p.Gender, TextBoxGender.Text, StringComparison.CurrentCultureIgnoreCase));
                    }

                    if (!string.IsNullOrEmpty(TextBoxTitle.Text))
                    {
                        predicate = predicate.Or(p => String.Equals(p.Title, TextBoxTitle.Text, StringComparison.CurrentCultureIgnoreCase));
                    }

                    if (!string.IsNullOrEmpty(TextBoxOccupation.Text))
                    {
                        predicate = predicate.Or(p => p.Occupation.ToLower() == TextBoxOccupation.Text.ToLower());
                    }

                    if (!string.IsNullOrEmpty(TextBoxGivenName.Text))
                    {
                        predicate = predicate.Or(p => p.GivenName.ToLower().Contains(TextBoxGivenName.Text.ToLower()));
                    }

                    if (!string.IsNullOrEmpty(TextBoxMiddleName.Text))
                    {
                        predicate = predicate.Or(p => p.MiddleInitial.ToLower().Contains(TextBoxMiddleName.Text.ToLower()));
                    }

                    if (!string.IsNullOrEmpty(TextBoxSurname.Text))
                    {
                        predicate = predicate.Or(p => p.Surname.ToLower().Contains(TextBoxSurname.Text.ToLower()));
                    }

                    if (!string.IsNullOrEmpty(TextBoxCompany.Text))
                    {
                        predicate = predicate.Or(p => p.Company.ToLower() == TextBoxCompany.Text.ToLower());
                    }

                    if (!string.IsNullOrEmpty(TextBoxBloodType.Text))
                    {
                        predicate = predicate.Or(p => p.BloodType.ToLower() == TextBoxBloodType.Text.ToLower());
                    }

                    if (!string.IsNullOrEmpty(TextBoxEmail.Text))
                    {
                        predicate = predicate.Or(p => p.EmailAddress.ToLower().Contains(TextBoxEmail.Text.ToLower()));
                    }

                    filteredPeople = filteredPeople.Where(predicate);

                    break;
            }

            //int limit;
            //if (int.TryParse(TextBoxLimitRows.Text, out limit))
            //{
            //    filteredPeople = filteredPeople.Take(limit);
            //}

            filteredPeople = filteredPeople.Take((int) ComboBoxLimitRows.SelectedValue);

            PeopleDataGrid.ItemsSource = filteredPeople.ToList();
            PeopleDataGrid.Items.Refresh();
        }

        private void OpenWindowExplorer(string dir)
        {
            Process process;
            process = Process.Start(dir);
        }

        
    }
}
