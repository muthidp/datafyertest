﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataFyerTest.Models;

namespace DataFyerTest
{
    public class InterviewLogic
    {
        public List<string> Headers { get; set; }

        public List<Person> People { get; set; }

        public InterviewLogic()
        {
            Initialize();
        }

        private void Initialize()
        {
            Headers = new List<string>();
            People = new List<Person>();
        }

        private void Reset()
        {
            Headers.Clear();
            People.Clear();
        }

        public void ReadFromCsv(string filePath)
        {
            Reset();
            Initialize();

            var data = ExcelHelper.csvParser(filePath);
            var excelHeaders = data.Cast<ArrayList>().ToList().First().ToArray().ToList();
            
            excelHeaders.ForEach(e => Headers.Add(e.ToString()));
            
            var people = data.Cast<ArrayList>().Skip(1).ToList();
            people.ForEach(p =>
            {
                var person = new Person()
                {
                    Gender = p[0].ToString().Trim(),
                    Title = p[1].ToString().Trim(),
                    Occupation = p[2].ToString().Trim(),
                    Company = p[3].ToString().Trim(),
                    GivenName = p[4].ToString().Trim(),
                    MiddleInitial = p[5].ToString().Trim(),
                    Surname = p[6].ToString().Trim(),
                    BloodType = p[7].ToString().Trim(),
                    EmailAddress = p[8].ToString().Trim(),
                };
                People.Add(person);
            });
        }


    }
}
