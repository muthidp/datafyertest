﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace DataFyerTest
{
    public class ExcelHelper
    {
        public static ArrayList xlsParserFromStream(Stream s, String sheet = "Sheet1")
        {
            var ret = new ArrayList();
            try
            {
                IWorkbook wb = new XSSFWorkbook(s);
                var sht = (XSSFSheet)wb.GetSheet(sheet);
                int rowCount = sht.PhysicalNumberOfRows;
                int colCount = sht.GetRow(0).PhysicalNumberOfCells;
                for (int i = 0; i < rowCount; i++)
                {
                    var row = new ArrayList();
                    for (int j = 0; j < colCount; j++)
                    {
                        var obj = sht.GetRow(i).GetCell(j);
                        row.Add(obj != null ? obj.ToString() : "");
                    }
                    ret.Add(row);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return ret;
        }

        public static ArrayList xlsParser(String filePath, String sheet = "Sheet1",
            int startColumn = 1, int? endColumn = null, int startRow = 0, int? endRow = null)
        {
            var ret = new ArrayList();
            try
            {
                IWorkbook wb = new XSSFWorkbook(new FileStream(filePath, FileMode.Open));
                var sht = (XSSFSheet)wb.GetSheet(sheet);
                int rowCount = endRow ?? sht.PhysicalNumberOfRows;
                int colCount = sht.GetRow(startRow).PhysicalNumberOfCells;
                for (int i = 0; i < rowCount; i++)
                {
                    var row = new ArrayList();
                    for (int j = (startColumn - 1); j < (endColumn ?? colCount); j++)
                    {
                        var cellValue = sht.GetRow(i).GetCell(j);
                        row.Add(cellValue != null ? cellValue.ToString() : "");
                    }
                    ret.Add(row);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return ret;
        }

        public static ArrayList csvParser(String filePath)
        {
            var ret = new ArrayList();
            try
            {
                String[] lines = File.ReadAllLines(filePath);
                foreach (String l in lines)
                {
                    String line = l;
                    if (line.Length > 0)
                    {
                        var row = new ArrayList();
                        String[] csvRow = line.Split(new[] { ',' });
                        foreach (String cell in csvRow)
                        {
                            row.Add(cell.Replace("\"", ""));
                        }
                        ret.Add(row);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return ret;
        }

        public static ArrayList parser(String extension, String filePath, String sheet = "Sheet")
        {
            var ret = new ArrayList();
            if (extension == ".csv")
            {
                ret = csvParser(filePath);
            }
            else if (extension == ".xls")
            {
                ret = xlsParser(filePath, sheet);
            }
            return ret;
        }

        public static byte[] exportXLS(String templatePath, ArrayList data, int startRow, int startColumn,
                                       String templateSheet = "Sheet1")
        {
            try
            {
                var fs = new FileStream(templatePath, FileMode.Open, FileAccess.Read);
                var templateWorkbook = new HSSFWorkbook(fs, true);

                var sheet = (HSSFSheet)templateWorkbook.GetSheet(templateSheet);

                for (int i = 0; i < data.Count; i++)
                {
                    var row = (ArrayList)data[i];
                    IRow xlsRow = sheet.CreateRow(i + startRow);
                    for (int j = 0; j < row.Count; j++)
                    {
                        xlsRow.CreateCell(j + startColumn).SetCellValue(row[j].ToString());
                    }
                }

                //sheet.ForceFormulaRecalculation = true;

                var ms = new MemoryStream();

                templateWorkbook.Write(ms);

                return ms.ToArray();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        public static byte[] exportCSV(ArrayList data)
        {
            String csv = "";
            foreach (ArrayList row in data)
            {
                foreach (String cell in row)
                {
                    csv += cell + ",";
                }
                csv += "\r\n";
            }
            var bytes = new byte[csv.Length * sizeof(char)];
            Buffer.BlockCopy(csv.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public static byte[] exportCSV(string data)
        {
            string csv = data;
            var bytes = new byte[csv.Length * sizeof(char)];
            Buffer.BlockCopy(csv.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
    }
}
