﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DataFyerTest.Models;

namespace DataFyerTest
{
    /// <summary>
    /// Interaction logic for PersonDialog.xaml
    /// </summary>
    public partial class PersonDialog : Window
    {
        public Person Person
        {
            get;
            set;
        }

        public bool IsSave;

        public PersonDialog()
        {
            InitializeComponent();
            IsSave = false;
            Person = new Person();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            var person = GetPerson();
            if (
                string.IsNullOrEmpty(person.Gender) ||
                string.IsNullOrEmpty(person.Title) ||
                string.IsNullOrEmpty(person.Occupation) ||
                string.IsNullOrEmpty(person.GivenName) ||
                string.IsNullOrEmpty(person.MiddleInitial) ||
                string.IsNullOrEmpty(person.Surname) ||
                string.IsNullOrEmpty(person.Company) ||
                string.IsNullOrEmpty(person.BloodType) ||
                string.IsNullOrEmpty(person.EmailAddress)
                )
            {
                MessageBox.Show("All fields cannot be empty");
                return;
            }

            IsSave = true;
            Close();
        }

        public Person GetPerson()
        {
            if (Person != null)
            {
                Person.Gender = textBoxGender.Text;
                Person.Title = textBoxTitle.Text;
                Person.Occupation = textBoxOccupation.Text;
                Person.GivenName = textBoxGivenName.Text;
                Person.MiddleInitial = textBoxMiddleName.Text;
                Person.Surname = textBoxSurname.Text;
                Person.Company = textBoxCompany.Text;
                Person.BloodType = textBoxBloodType.Text;
                Person.EmailAddress = textBoxEmail.Text;
            }
            return Person;
        }

        private void button_Copy_Click(object sender, RoutedEventArgs e)
        {
            IsSave = false;
            Close();
        }
    }
}
